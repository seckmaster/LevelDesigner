﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LevelDesigner
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox5.Text = openFileDialog1.SafeFileName;
                Image bg = Image.FromFile("../../res/" + openFileDialog1.SafeFileName);
                textBox1.Text = "" + bg.Width;
                textBox2.Text = "" + bg.Height;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            o.w = Int32.Parse(textBox1.Text);
            o.h = Int32.Parse(textBox2.Text);
            o.x = Int32.Parse(textBox3.Text);
            o.y = Int32.Parse(textBox4.Text);
            o.image = textBox5.Text;
            o.objectType = (E_BODY_TYPE)Int32.Parse("" + comboBox1.Text[0]);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        GameData o = new GameData();
        public GameData Value
        {
            get { return o; }
        }

    }
}
