﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.IO;

namespace LevelDesigner
{
    public partial class Form1 : Form
    {
        // loaded from file
        string fileLocation = "";

        // mouse position
        Point mpos = new Point();
        // drag/droping from panel3 or from inside panel 1
        bool dragInside = false;
        // enable/disable dragging
        bool dragging = false;
        // width/height of one cell
        const int WIDTH = 40;
        const int HEIGHT = 40;
        // background image
        string image = "";

        // currently selected button
        myButton selected = null;
        // coppied button
        myButton copied = null;
        // where to past button
        Point copyHere = new Point();

        string[] objectTypes = 
        {
            "EBT_undefined",
	        "EBT_tossingBall",
	        "EBT_enemy",
	        "EBT_sceneObject",
	        "EBT_groundObject",
	        "EBT_obstacle"
        };

        string[] shapeTypes = 
        {
            "EST_rect",
            "EST_circle",
            "EST_triangle"
        };

        // returns a copy of a button b
        myButton createCopy(myButton b, Point location)
        {
            myButton x = (myButton)b.Clone();

            // add events to button
            x.MouseDown += new MouseEventHandler(objekt_MouseDown);

            // set position where mouse was released
            Point pos = new Point(location.X, location.Y);
            pos = panel1.PointToClient(pos);

            x.Location = pos;
            x.data.x = x.Location.X + x.Width / 2;
            x.data.y = x.Location.Y + x.Height / 2;

            return x;
        }

        public Form1()
        {
            InitializeComponent();

            foreach (Control b in panel3.Controls)
            {
                if (b.GetType() == typeof(myButton))
                {
                    b.MouseDown += new MouseEventHandler(objekt_MouseDown);
                    (b as myButton).data.w = b.Width;
                    (b as myButton).data.h = b.Height;
                }
            }

            panel1.Focus();
            panel1.BackgroundImageLayout = ImageLayout.Stretch;
            panel1.AllowDrop = true;
            panel3.AllowDrop = true;
        }

        private void loadFromFile(string p)
        {
            //List<GameData> gdata = IO.LoadFromFile(p, ref image);
            List<GameData> gdata = IO.LoadFromXML(p, checkBox1.Checked);
            panel1.Controls.Clear();

            foreach (GameData g in gdata)
            {
                myButton x = new myButton();
                x.data = g;

                x.Size = new Size(g.w, g.h);
                x.Location = new Point(g.x - g.w / 2, g.y - g.h / 2);
                x.BackColor = g.c;
                x.isSet = true;
                if(g.image != "")
                    x.BackgroundImage = Image.FromFile("../../res/" + g.image);
                x.BackColor = Color.Transparent;

                x.MouseDown += new MouseEventHandler(objekt_MouseDown);

                panel1.Controls.Add(x);
            }
            if (image == "") panel1.BackgroundImage = null;
            else
                panel1.BackgroundImage = Image.FromFile("../../res/" + image);
            fileLocation = p;
        }

        // drop button in panel 1
        private void panel1_DragDrop(object sender, DragEventArgs e)
        {
            myButton b = (myButton)e.Data.GetData(typeof(myButton));

            // if object is drag/droped from inside of the panel
            // or from panel 3
            if (!dragInside)
            {
                // create a copy of button
                myButton x = createCopy(b, new Point(e.X - b.Width / 2, e.Y - b.Height / 2));

                // update data on panel 2
                setPanel2(x.data);

                // add button to panel
                panel1.Controls.Add(x);

                selected = x;
                dragInside = true;
            }
            // moving object around
            else
            {
                // set position where mouse was released
                Point loc = panel1.PointToClient(new Point(e.X, e.Y));
                b.Location = panel1.PointToClient(new Point(e.X - b.Width / 2, e.Y - b.Height / 2));
                b.data.x = loc.X;
                b.data.y = loc.Y;

                // add button to panel
                panel1.Controls.Add(b);
            }
        }

        private void panel1_DragEnter(object sender, DragEventArgs e)
        {
            // dragging inside if CTRL is hold
            if (dragInside)
            {
                if ((e.KeyState & 8) == 8) e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.Move;
            }
        }

        private void objekt_MouseDown(object sender, EventArgs e)
        {
            myButton b = (sender as myButton);
            b.DoDragDrop(b, DragDropEffects.Move);

            // display data in data panel
            if (b.isSet)
            {
                setPanel2(b.data);
                selected = b;
            }
        }

        private void panel1_MouseLeave(object sender, EventArgs e)
        {
            dragInside = false;
        }

        private void panel1_MouseEnter(object sender, EventArgs e)
        {
            dragInside = true;
        }

        // confirm changes
        private void button1_Click(object sender, EventArgs e)
        {
            if (selected != null)
            {
                // update button data
                selected.data.x = Int32.Parse(t_posX.Text);
                selected.data.y = Int32.Parse(t_posY.Text);
                selected.data.w = Int32.Parse(t_width.Text);
                selected.data.h = Int32.Parse(t_height.Text);
                selected.data.dynamic = Boolean.Parse(t_dynamic.Text);

                selected.Location = new Point(selected.data.x - selected.Width / 2, selected.data.y - selected.Height / 2);
                selected.Size = new Size(selected.data.w, selected.data.h);
                selected.BackColor = selected.data.c;
            }
        }

        // shrani v datoteko
        private void shraniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // iz panela shranimo vse podatke o objektih v polje
            List<GameData> collection = new List<GameData>();

            // dodamo še sliko ozadja kot objekt (sprite)
            if (!image.Equals(""))
            {
                GameData bgSprite = new GameData();
                bgSprite.x = panel1.Width / 2;
                bgSprite.y = panel1.Height / 2;
                bgSprite.w = panel1.Width;
                bgSprite.h = panel1.Height;
                bgSprite.image = image;
                bgSprite.objectType = E_BODY_TYPE.EBT_sceneObject;
                collection.Add(bgSprite);
            }

            foreach(myButton i in panel1.Controls) 
            {
                collection.Add(i.data);
            }

            if (!fileLocation.Equals(""))
            {
                IO.SaveToXML(collection, fileLocation, checkBox1.Checked);
            }

            else if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                IO.SaveToXML(collection, saveFileDialog1.FileName, checkBox1.Checked);
                fileLocation = saveFileDialog1.FileName;
                Text = Text + " | " + saveFileDialog1.FileName;
            }
        }


        //shrani kot
        private void shraniKotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileLocation = saveFileDialog1.FileName;
                shraniToolStripMenuItem_Click(sender, e);
            }
        }

        // naloži iz dat.
        private void naložiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                loadFromFile(openFileDialog1.FileName);
                Text = Text + " | " + openFileDialog1.FileName;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            selected = null;
            dragging = false;
            clearPanel2();
            copyHere = new Point(e.X, e.Y);
        }

        void clearPanel2()
        {
            t_posX.Text = "";
            t_posY.Text = "";
            t_width.Text = "";
            t_height.Text = "";
            t_gravity.Text = "";
            t_dynamic.Text = "";
            t_type.Text = "";
            t_shape.Text = "";
            button2.BackColor = Color.White;
        }

        void setPanel2(GameData data)
        {
            t_posX.Text = "" + data.x;
            t_posY.Text = "" + data.y;
            t_width.Text = "" + data.w;
            t_height.Text = "" + data.h;
            t_gravity.Text = "" + data.gravity;
            t_dynamic.Text = "" + data.dynamic;
            t_type.Text = objectTypes[(int)data.objectType];
            t_shape.Text = shapeTypes[(int)data.shapeType];
            button2.BackColor = data.c;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (selected != null)
            {
                if (colorDialog1.ShowDialog() == DialogResult.OK)
                {
                    button2.BackColor = colorDialog1.Color;
                    selected.data.c = colorDialog1.Color;
                    selected.BackColor = colorDialog1.Color;
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            int yn = panel1.Height / HEIGHT;

            for (int i = 0; i < yn; i++)
            {
                g.DrawLine(Pens.Gray, 0, i * HEIGHT + HEIGHT, 2000, i * HEIGHT + HEIGHT);
            }
            for (int i = 0; i < 100; i++)
            {
                g.DrawLine(Pens.Gray, i * WIDTH + WIDTH, 0, i * WIDTH + WIDTH, panel1.Height);
            }
        }

        private void panel1_MouseMove_1(object sender, MouseEventArgs e)
        {
            mpos.X = e.X;
            mpos.Y = e.Y;
            toolStripStatusLabel1.Text = "" + mpos.X;
            toolStripStatusLabel2.Text = "" + mpos.Y;

            if(selected != null && dragging)
                selected.Location = new Point(mpos.X, mpos.Y);
        }

        // background
        private void ozadjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(DialogResult.OK == openFileDialog1.ShowDialog()) 
            {
                panel1.BackgroundImage = Image.FromFile(openFileDialog1.FileName);
                image = System.IO.Path.GetFileName(openFileDialog1.FileName);
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {                
            // CTRL + V /// paste button 
            if (e.Control && e.KeyCode == Keys.V && selected == null && copied != null)
            {
                copied.Location = copyHere;
                copied.data.x = copied.Location.X;
                copied.data.y = copied.Location.Y;

                panel1.Controls.Add(copied);

                //copied = createCopy(copied, copied.Location);
            }
            else if (selected != null)
            {
                // CTRL + C /// copy button
                if (e.Control && e.KeyCode == Keys.C)
                {
                    copied = createCopy(selected, new Point());
                }
                // DELETE /// delete button
                else if (e.KeyCode == Keys.Delete)
                {
                    panel1.Controls.Remove(selected);
                    clearPanel2();
                }
                // A /// move left
                else if (e.KeyCode == Keys.A)
                {
                    selected.Location = new Point(selected.Location.X - 1, selected.Location.Y);
                    selected.data.x = selected.Location.X + selected.Width / 2;
                    selected.data.y = selected.Location.Y + selected.Height / 2;
                    setPanel2(selected.data);
                }
                // D /// move right
                else if (e.KeyCode == Keys.D)
                {
                    selected.Location = new Point(selected.Location.X + 1, selected.Location.Y);
                    selected.data.x = selected.Location.X + selected.Width / 2;
                    selected.data.y = selected.Location.Y + selected.Height / 2;
                    setPanel2(selected.data);
                }
                // W /// move up
                else if (e.KeyCode == Keys.W)
                {
                    selected.Location = new Point(selected.Location.X, selected.Location.Y - 1);
                    selected.data.x = selected.Location.X + selected.Width / 2;
                    selected.data.y = selected.Location.Y + selected.Height / 2;
                    setPanel2(selected.data);
                }
                // S /// move down
                else if (e.KeyCode == Keys.S)
                {
                    selected.Location = new Point(selected.Location.X, selected.Location.Y + 1);
                    selected.data.x = selected.Location.X + selected.Width / 2;
                    selected.data.y = selected.Location.Y + selected.Height / 2;
                    setPanel2(selected.data);
                }
                
            }
        }

        private void t_gravity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (selected != null)
            {
                selected.data.gravity = (t_gravity.Text == "True") ? true : false;
            }
        }

        private void t_dynamic_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (selected != null)
            {
                selected.data.dynamic = (t_dynamic.Text == "True") ? true : false;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (selected != null)
            {
                int id = -1;
                switch(t_type.Text) 
                {
                    case "EBT_undefined":   id = 0; break;
                    case "EBT_tossingBall": id = 1; break;
                    case "EBT_enemy":       id = 2; break;
                    case "EBT_sceneObject": id = 3; break;
                    case "EBT_groundObject":id = 4; break;
                    case "EBT_obstacle":    id = 5; break;
                }
                selected.data.objectType = (E_BODY_TYPE)id;
            }
        }

        // novo
        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fileLocation = "";

            mpos = new Point();
            dragInside = false;
            dragging = false;
            image = "";

            panel1.Controls.Clear();
        }

        private void dodajObjektToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameData newObject = new GameData();
            using (Form2 add = new Form2())
            {
                if (add.ShowDialog() == DialogResult.OK)
                {
                    GameData o = add.Value;
                    myButton b = new myButton();
                    b.data = o;
                    b.Location = new Point(o.x, o.y);
                    b.Size = new Size(o.w, o.h);
                    b.Image = Image.FromFile("../../res/" + o.image); 
                    b.MouseDown += new MouseEventHandler(objekt_MouseDown);
                    b.isSet = true;

                    // update data on panel 2
                    setPanel2(b.data);

                    // add button to panel
                    panel1.Controls.Add(b);
                }
            }
        }


    }

}
