﻿using System.Windows.Forms;
namespace LevelDesigner
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            LevelDesigner.GameData gameData1 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData2 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData3 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData4 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData5 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData6 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData7 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData8 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData9 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData10 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData11 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData12 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData13 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData14 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData15 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData16 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData17 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData18 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData19 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData20 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData21 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData22 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData23 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData24 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData25 = new LevelDesigner.GameData();
            LevelDesigner.GameData gameData26 = new LevelDesigner.GameData();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.datotekaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shraniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shraniKotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.naložiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodatnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ozadjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajObjektToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.t_shape = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.t_type = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.t_dynamic = new System.Windows.Forms.ComboBox();
            this.t_gravity = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.t_height = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.t_width = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.t_posY = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.t_posX = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.myButton8 = new LevelDesigner.myButton();
            this.myButton22 = new LevelDesigner.myButton();
            this.myButton19 = new LevelDesigner.myButton();
            this.myButton18 = new LevelDesigner.myButton();
            this.myButton17 = new LevelDesigner.myButton();
            this.myButton20 = new LevelDesigner.myButton();
            this.myButton23 = new LevelDesigner.myButton();
            this.myButton24 = new LevelDesigner.myButton();
            this.myButton28 = new LevelDesigner.myButton();
            this.myButton29 = new LevelDesigner.myButton();
            this.myButton30 = new LevelDesigner.myButton();
            this.myButton31 = new LevelDesigner.myButton();
            this.myButton32 = new LevelDesigner.myButton();
            this.myButton14 = new LevelDesigner.myButton();
            this.myButton15 = new LevelDesigner.myButton();
            this.myButton16 = new LevelDesigner.myButton();
            this.myButton10 = new LevelDesigner.myButton();
            this.myButton11 = new LevelDesigner.myButton();
            this.myButton12 = new LevelDesigner.myButton();
            this.myButton7 = new LevelDesigner.myButton();
            this.myButton6 = new LevelDesigner.myButton();
            this.myButton5 = new LevelDesigner.myButton();
            this.myButton3 = new LevelDesigner.myButton();
            this.myButton4 = new LevelDesigner.myButton();
            this.myButton2 = new LevelDesigner.myButton();
            this.myButton1 = new LevelDesigner.myButton();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datotekaToolStripMenuItem,
            this.dodatnoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1225, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // datotekaToolStripMenuItem
            // 
            this.datotekaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shraniToolStripMenuItem,
            this.shraniKotToolStripMenuItem,
            this.naložiToolStripMenuItem,
            this.novoToolStripMenuItem});
            this.datotekaToolStripMenuItem.Name = "datotekaToolStripMenuItem";
            this.datotekaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.datotekaToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.datotekaToolStripMenuItem.Text = "Datoteka";
            // 
            // shraniToolStripMenuItem
            // 
            this.shraniToolStripMenuItem.Name = "shraniToolStripMenuItem";
            this.shraniToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.shraniToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.shraniToolStripMenuItem.Text = "Shrani";
            this.shraniToolStripMenuItem.Click += new System.EventHandler(this.shraniToolStripMenuItem_Click);
            // 
            // shraniKotToolStripMenuItem
            // 
            this.shraniKotToolStripMenuItem.Name = "shraniKotToolStripMenuItem";
            this.shraniKotToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.S)));
            this.shraniKotToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.shraniKotToolStripMenuItem.Text = "Shrani kot";
            this.shraniKotToolStripMenuItem.Click += new System.EventHandler(this.shraniKotToolStripMenuItem_Click);
            // 
            // naložiToolStripMenuItem
            // 
            this.naložiToolStripMenuItem.Name = "naložiToolStripMenuItem";
            this.naložiToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.naložiToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.naložiToolStripMenuItem.Text = "Naloži";
            this.naložiToolStripMenuItem.Click += new System.EventHandler(this.naložiToolStripMenuItem_Click);
            // 
            // novoToolStripMenuItem
            // 
            this.novoToolStripMenuItem.Name = "novoToolStripMenuItem";
            this.novoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.novoToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.novoToolStripMenuItem.Text = "Novo";
            this.novoToolStripMenuItem.Click += new System.EventHandler(this.novoToolStripMenuItem_Click);
            // 
            // dodatnoToolStripMenuItem
            // 
            this.dodatnoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ozadjeToolStripMenuItem,
            this.dodajObjektToolStripMenuItem});
            this.dodatnoToolStripMenuItem.Name = "dodatnoToolStripMenuItem";
            this.dodatnoToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.dodatnoToolStripMenuItem.Text = "Dodatno";
            // 
            // ozadjeToolStripMenuItem
            // 
            this.ozadjeToolStripMenuItem.Name = "ozadjeToolStripMenuItem";
            this.ozadjeToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.ozadjeToolStripMenuItem.Text = "Nastavi ozadje";
            this.ozadjeToolStripMenuItem.Click += new System.EventHandler(this.ozadjeToolStripMenuItem_Click);
            // 
            // dodajObjektToolStripMenuItem
            // 
            this.dodajObjektToolStripMenuItem.Name = "dodajObjektToolStripMenuItem";
            this.dodajObjektToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.dodajObjektToolStripMenuItem.Text = "Dodaj objekt";
            this.dodajObjektToolStripMenuItem.Click += new System.EventHandler(this.dodajObjektToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 648);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1225, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // panel1
            // 
            this.panel1.AllowDrop = true;
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(12, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(960, 600);
            this.panel1.TabIndex = 2;
            this.panel1.DragDrop += new System.Windows.Forms.DragEventHandler(this.panel1_DragDrop);
            this.panel1.DragEnter += new System.Windows.Forms.DragEventHandler(this.panel1_DragEnter);
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseEnter += new System.EventHandler(this.panel1_MouseEnter);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove_1);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.t_shape);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.t_type);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.t_dynamic);
            this.panel2.Controls.Add(this.t_gravity);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.t_height);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.t_width);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.t_posY);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.t_posX);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(979, 374);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(234, 265);
            this.panel2.TabIndex = 4;
            // 
            // t_shape
            // 
            this.t_shape.FormattingEnabled = true;
            this.t_shape.Items.AddRange(new object[] {
            "EST_rect",
            "EST_circle",
            "EST_triangle"});
            this.t_shape.Location = new System.Drawing.Point(78, 213);
            this.t_shape.Name = "t_shape";
            this.t_shape.Size = new System.Drawing.Size(97, 21);
            this.t_shape.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 216);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Oblika:";
            // 
            // t_type
            // 
            this.t_type.FormattingEnabled = true;
            this.t_type.Items.AddRange(new object[] {
            "EBT_undefined",
            "EBT_tossingBall",
            "EBT_enemy",
            "EBT_sceneObject",
            "EBT_groundObject",
            "EBT_obstacle"});
            this.t_type.Location = new System.Drawing.Point(78, 182);
            this.t_type.Name = "t_type";
            this.t_type.Size = new System.Drawing.Size(97, 21);
            this.t_type.TabIndex = 21;
            this.t_type.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 185);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Tip telesa:";
            // 
            // t_dynamic
            // 
            this.t_dynamic.FormattingEnabled = true;
            this.t_dynamic.Items.AddRange(new object[] {
            "True",
            "False"});
            this.t_dynamic.Location = new System.Drawing.Point(78, 144);
            this.t_dynamic.Name = "t_dynamic";
            this.t_dynamic.Size = new System.Drawing.Size(97, 21);
            this.t_dynamic.TabIndex = 19;
            this.t_dynamic.SelectedIndexChanged += new System.EventHandler(this.t_dynamic_SelectedIndexChanged);
            // 
            // t_gravity
            // 
            this.t_gravity.FormattingEnabled = true;
            this.t_gravity.Items.AddRange(new object[] {
            "True",
            "False"});
            this.t_gravity.Location = new System.Drawing.Point(78, 112);
            this.t_gravity.Name = "t_gravity";
            this.t_gravity.Size = new System.Drawing.Size(97, 21);
            this.t_gravity.TabIndex = 18;
            this.t_gravity.SelectedIndexChanged += new System.EventHandler(this.t_gravity_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 147);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Dinamičen:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 115);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Gravitacija:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(78, 243);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 23);
            this.button2.TabIndex = 12;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 281);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Potrdi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // t_height
            // 
            this.t_height.Location = new System.Drawing.Point(152, 75);
            this.t_height.Name = "t_height";
            this.t_height.Size = new System.Drawing.Size(43, 20);
            this.t_height.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(107, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Širina:";
            // 
            // t_width
            // 
            this.t_width.Location = new System.Drawing.Point(54, 75);
            this.t_width.Name = "t_width";
            this.t_width.Size = new System.Drawing.Size(45, 20);
            this.t_width.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Dolžina:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Barva:";
            // 
            // t_posY
            // 
            this.t_posY.Location = new System.Drawing.Point(152, 34);
            this.t_posY.Name = "t_posY";
            this.t_posY.Size = new System.Drawing.Size(45, 20);
            this.t_posY.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Pos. y:";
            // 
            // t_posX
            // 
            this.t_posX.Location = new System.Drawing.Point(54, 34);
            this.t_posX.Name = "t_posX";
            this.t_posX.Size = new System.Drawing.Size(45, 20);
            this.t_posX.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pos. x:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(9, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lastnosti:";
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.Controls.Add(this.myButton8);
            this.panel3.Controls.Add(this.myButton22);
            this.panel3.Controls.Add(this.checkBox1);
            this.panel3.Controls.Add(this.myButton19);
            this.panel3.Controls.Add(this.myButton18);
            this.panel3.Controls.Add(this.myButton17);
            this.panel3.Controls.Add(this.myButton20);
            this.panel3.Controls.Add(this.myButton23);
            this.panel3.Controls.Add(this.myButton24);
            this.panel3.Controls.Add(this.myButton28);
            this.panel3.Controls.Add(this.myButton29);
            this.panel3.Controls.Add(this.myButton30);
            this.panel3.Controls.Add(this.myButton31);
            this.panel3.Controls.Add(this.myButton32);
            this.panel3.Controls.Add(this.myButton14);
            this.panel3.Controls.Add(this.myButton15);
            this.panel3.Controls.Add(this.myButton16);
            this.panel3.Controls.Add(this.myButton10);
            this.panel3.Controls.Add(this.myButton11);
            this.panel3.Controls.Add(this.myButton12);
            this.panel3.Controls.Add(this.myButton7);
            this.panel3.Controls.Add(this.myButton6);
            this.panel3.Controls.Add(this.myButton5);
            this.panel3.Controls.Add(this.myButton3);
            this.panel3.Controls.Add(this.myButton4);
            this.panel3.Controls.Add(this.myButton2);
            this.panel3.Controls.Add(this.myButton1);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Location = new System.Drawing.Point(982, 39);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(235, 316);
            this.panel3.TabIndex = 5;
            this.panel3.MouseEnter += new System.EventHandler(this.panel1_MouseEnter);
            this.panel3.MouseLeave += new System.EventHandler(this.panel1_MouseLeave);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(114, 137);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(45, 17);
            this.checkBox1.TabIndex = 48;
            this.checkBox1.Text = "GUI";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(6, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 19;
            this.label7.Text = "Dodaj objekt:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // myButton8
            // 
            this.myButton8.BackColor = System.Drawing.Color.Transparent;
            this.myButton8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton8.BackgroundImage")));
            this.myButton8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton8.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData1.angle = 0;
            gameData1.c = System.Drawing.Color.DarkGray;
            gameData1.dynamic = true;
            gameData1.h = 40;
            gameData1.hp = 70;
            gameData1.image = "steklo_krog.png";
            gameData1.nDamage = 3;
            gameData1.nDestroy = 3;
            gameData1.nImpact = 8;
            gameData1.numberOftextures = 4;
            gameData1.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData1.onDamage = "glass damage a";
            gameData1.onDestroy = "glass destroy a";
            gameData1.onImpact = "glass collision a";
            gameData1.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_circle;
            gameData1.w = 40;
            gameData1.x = 0;
            gameData1.y = 0;
            gameData1.z = 0;
            this.myButton8.data = gameData1;
            this.myButton8.FlatAppearance.BorderSize = 0;
            this.myButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton8.isSet = false;
            this.myButton8.Location = new System.Drawing.Point(149, 225);
            this.myButton8.Name = "myButton8";
            this.myButton8.Size = new System.Drawing.Size(45, 45);
            this.myButton8.TabIndex = 50;
            this.myButton8.UseVisualStyleBackColor = false;
            // 
            // myButton22
            // 
            this.myButton22.BackColor = System.Drawing.Color.Transparent;
            this.myButton22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton22.BackgroundImage")));
            this.myButton22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton22.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData2.angle = 0;
            gameData2.c = System.Drawing.Color.DarkGray;
            gameData2.dynamic = true;
            gameData2.h = 60;
            gameData2.hp = 100;
            gameData2.image = "monster.png";
            gameData2.nDamage = 0;
            gameData2.nDestroy = 0;
            gameData2.nImpact = 0;
            gameData2.numberOftextures = 1;
            gameData2.objectType = LevelDesigner.E_BODY_TYPE.EBT_enemy;
            gameData2.onDamage = null;
            gameData2.onDestroy = null;
            gameData2.onImpact = null;
            gameData2.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData2.w = 52;
            gameData2.x = 0;
            gameData2.y = 0;
            gameData2.z = 0;
            this.myButton22.data = gameData2;
            this.myButton22.FlatAppearance.BorderSize = 0;
            this.myButton22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton22.isSet = false;
            this.myButton22.Location = new System.Drawing.Point(29, 347);
            this.myButton22.Name = "myButton22";
            this.myButton22.Size = new System.Drawing.Size(52, 60);
            this.myButton22.TabIndex = 49;
            this.myButton22.UseVisualStyleBackColor = false;
            // 
            // myButton19
            // 
            this.myButton19.BackColor = System.Drawing.Color.Transparent;
            this.myButton19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton19.BackgroundImage")));
            this.myButton19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton19.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData3.angle = 0;
            gameData3.c = System.Drawing.Color.DarkGray;
            gameData3.dynamic = false;
            gameData3.h = 40;
            gameData3.hp = 100;
            gameData3.image = "back.png";
            gameData3.nDamage = 0;
            gameData3.nDestroy = 0;
            gameData3.nImpact = 0;
            gameData3.numberOftextures = 0;
            gameData3.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData3.onDamage = null;
            gameData3.onDestroy = null;
            gameData3.onImpact = null;
            gameData3.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData3.w = 40;
            gameData3.x = 0;
            gameData3.y = 0;
            gameData3.z = 0;
            this.myButton19.data = gameData3;
            this.myButton19.FlatAppearance.BorderSize = 0;
            this.myButton19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton19.isSet = false;
            this.myButton19.Location = new System.Drawing.Point(9, 561);
            this.myButton19.Name = "myButton19";
            this.myButton19.Size = new System.Drawing.Size(90, 77);
            this.myButton19.TabIndex = 46;
            this.myButton19.UseVisualStyleBackColor = false;
            // 
            // myButton18
            // 
            this.myButton18.BackColor = System.Drawing.Color.Transparent;
            this.myButton18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton18.BackgroundImage")));
            this.myButton18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton18.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData4.angle = 0;
            gameData4.c = System.Drawing.Color.DarkGray;
            gameData4.dynamic = false;
            gameData4.h = 40;
            gameData4.hp = 100;
            gameData4.image = "play";
            gameData4.nDamage = 0;
            gameData4.nDestroy = 0;
            gameData4.nImpact = 0;
            gameData4.numberOftextures = 0;
            gameData4.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData4.onDamage = null;
            gameData4.onDestroy = null;
            gameData4.onImpact = null;
            gameData4.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData4.w = 40;
            gameData4.x = 0;
            gameData4.y = 0;
            gameData4.z = 0;
            this.myButton18.data = gameData4;
            this.myButton18.FlatAppearance.BorderSize = 0;
            this.myButton18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton18.isSet = false;
            this.myButton18.Location = new System.Drawing.Point(97, 909);
            this.myButton18.Name = "myButton18";
            this.myButton18.Size = new System.Drawing.Size(80, 80);
            this.myButton18.TabIndex = 45;
            this.myButton18.UseVisualStyleBackColor = false;
            // 
            // myButton17
            // 
            this.myButton17.BackColor = System.Drawing.Color.Transparent;
            this.myButton17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton17.BackgroundImage")));
            this.myButton17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton17.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData5.angle = 0;
            gameData5.c = System.Drawing.Color.DarkGray;
            gameData5.dynamic = false;
            gameData5.h = 40;
            gameData5.hp = 100;
            gameData5.image = "level9.png";
            gameData5.nDamage = 0;
            gameData5.nDestroy = 0;
            gameData5.nImpact = 0;
            gameData5.numberOftextures = 0;
            gameData5.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData5.onDamage = null;
            gameData5.onDestroy = null;
            gameData5.onImpact = null;
            gameData5.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData5.w = 40;
            gameData5.x = 0;
            gameData5.y = 0;
            gameData5.z = 0;
            this.myButton17.data = gameData5;
            this.myButton17.FlatAppearance.BorderSize = 0;
            this.myButton17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton17.isSet = false;
            this.myButton17.Location = new System.Drawing.Point(11, 909);
            this.myButton17.Name = "myButton17";
            this.myButton17.Size = new System.Drawing.Size(80, 80);
            this.myButton17.TabIndex = 44;
            this.myButton17.UseVisualStyleBackColor = false;
            // 
            // myButton20
            // 
            this.myButton20.BackColor = System.Drawing.Color.Transparent;
            this.myButton20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton20.BackgroundImage")));
            this.myButton20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton20.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData6.angle = 0;
            gameData6.c = System.Drawing.Color.DarkGray;
            gameData6.dynamic = false;
            gameData6.h = 40;
            gameData6.hp = 100;
            gameData6.image = "level7.png";
            gameData6.nDamage = 0;
            gameData6.nDestroy = 0;
            gameData6.nImpact = 0;
            gameData6.numberOftextures = 0;
            gameData6.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData6.onDamage = null;
            gameData6.onDestroy = null;
            gameData6.onImpact = null;
            gameData6.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData6.w = 40;
            gameData6.x = 0;
            gameData6.y = 0;
            gameData6.z = 0;
            this.myButton20.data = gameData6;
            this.myButton20.FlatAppearance.BorderSize = 0;
            this.myButton20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton20.isSet = false;
            this.myButton20.Location = new System.Drawing.Point(11, 825);
            this.myButton20.Name = "myButton20";
            this.myButton20.Size = new System.Drawing.Size(80, 80);
            this.myButton20.TabIndex = 43;
            this.myButton20.UseVisualStyleBackColor = false;
            // 
            // myButton23
            // 
            this.myButton23.BackColor = System.Drawing.Color.Transparent;
            this.myButton23.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton23.BackgroundImage")));
            this.myButton23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton23.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData7.angle = 0;
            gameData7.c = System.Drawing.Color.DarkGray;
            gameData7.dynamic = false;
            gameData7.h = 40;
            gameData7.hp = 100;
            gameData7.image = "level8.png";
            gameData7.nDamage = 0;
            gameData7.nDestroy = 0;
            gameData7.nImpact = 0;
            gameData7.numberOftextures = 0;
            gameData7.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData7.onDamage = null;
            gameData7.onDestroy = null;
            gameData7.onImpact = null;
            gameData7.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData7.w = 40;
            gameData7.x = 0;
            gameData7.y = 0;
            gameData7.z = 0;
            this.myButton23.data = gameData7;
            this.myButton23.FlatAppearance.BorderSize = 0;
            this.myButton23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton23.isSet = false;
            this.myButton23.Location = new System.Drawing.Point(97, 825);
            this.myButton23.Name = "myButton23";
            this.myButton23.Size = new System.Drawing.Size(80, 80);
            this.myButton23.TabIndex = 42;
            this.myButton23.UseVisualStyleBackColor = false;
            // 
            // myButton24
            // 
            this.myButton24.BackColor = System.Drawing.Color.Transparent;
            this.myButton24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton24.BackgroundImage")));
            this.myButton24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton24.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData8.angle = 0;
            gameData8.c = System.Drawing.Color.DarkGray;
            gameData8.dynamic = false;
            gameData8.h = 40;
            gameData8.hp = 100;
            gameData8.image = "level3.png";
            gameData8.nDamage = 0;
            gameData8.nDestroy = 0;
            gameData8.nImpact = 0;
            gameData8.numberOftextures = 0;
            gameData8.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData8.onDamage = null;
            gameData8.onDestroy = null;
            gameData8.onImpact = null;
            gameData8.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData8.w = 40;
            gameData8.x = 0;
            gameData8.y = 0;
            gameData8.z = 0;
            this.myButton24.data = gameData8;
            this.myButton24.FlatAppearance.BorderSize = 0;
            this.myButton24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton24.isSet = false;
            this.myButton24.Location = new System.Drawing.Point(11, 653);
            this.myButton24.Name = "myButton24";
            this.myButton24.Size = new System.Drawing.Size(80, 80);
            this.myButton24.TabIndex = 41;
            this.myButton24.UseVisualStyleBackColor = false;
            // 
            // myButton28
            // 
            this.myButton28.BackColor = System.Drawing.Color.Transparent;
            this.myButton28.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton28.BackgroundImage")));
            this.myButton28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton28.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData9.angle = 0;
            gameData9.c = System.Drawing.Color.DarkGray;
            gameData9.dynamic = false;
            gameData9.h = 40;
            gameData9.hp = 100;
            gameData9.image = "level5.png";
            gameData9.nDamage = 0;
            gameData9.nDestroy = 0;
            gameData9.nImpact = 0;
            gameData9.numberOftextures = 0;
            gameData9.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData9.onDamage = null;
            gameData9.onDestroy = null;
            gameData9.onImpact = null;
            gameData9.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData9.w = 40;
            gameData9.x = 0;
            gameData9.y = 0;
            gameData9.z = 0;
            this.myButton28.data = gameData9;
            this.myButton28.FlatAppearance.BorderSize = 0;
            this.myButton28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton28.isSet = false;
            this.myButton28.Location = new System.Drawing.Point(11, 739);
            this.myButton28.Name = "myButton28";
            this.myButton28.Size = new System.Drawing.Size(80, 80);
            this.myButton28.TabIndex = 40;
            this.myButton28.UseVisualStyleBackColor = false;
            // 
            // myButton29
            // 
            this.myButton29.BackColor = System.Drawing.Color.Transparent;
            this.myButton29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton29.BackgroundImage")));
            this.myButton29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton29.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData10.angle = 0;
            gameData10.c = System.Drawing.Color.DarkGray;
            gameData10.dynamic = false;
            gameData10.h = 40;
            gameData10.hp = 100;
            gameData10.image = "level6.png";
            gameData10.nDamage = 0;
            gameData10.nDestroy = 0;
            gameData10.nImpact = 0;
            gameData10.numberOftextures = 0;
            gameData10.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData10.onDamage = null;
            gameData10.onDestroy = null;
            gameData10.onImpact = null;
            gameData10.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData10.w = 40;
            gameData10.x = 0;
            gameData10.y = 0;
            gameData10.z = 0;
            this.myButton29.data = gameData10;
            this.myButton29.FlatAppearance.BorderSize = 0;
            this.myButton29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton29.isSet = false;
            this.myButton29.Location = new System.Drawing.Point(97, 739);
            this.myButton29.Name = "myButton29";
            this.myButton29.Size = new System.Drawing.Size(80, 80);
            this.myButton29.TabIndex = 39;
            this.myButton29.UseVisualStyleBackColor = false;
            // 
            // myButton30
            // 
            this.myButton30.BackColor = System.Drawing.Color.Transparent;
            this.myButton30.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton30.BackgroundImage")));
            this.myButton30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton30.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData11.angle = 0;
            gameData11.c = System.Drawing.Color.DarkGray;
            gameData11.dynamic = false;
            gameData11.h = 40;
            gameData11.hp = 100;
            gameData11.image = "level4.png";
            gameData11.nDamage = 0;
            gameData11.nDestroy = 0;
            gameData11.nImpact = 0;
            gameData11.numberOftextures = 0;
            gameData11.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData11.onDamage = null;
            gameData11.onDestroy = null;
            gameData11.onImpact = null;
            gameData11.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData11.w = 40;
            gameData11.x = 0;
            gameData11.y = 0;
            gameData11.z = 0;
            this.myButton30.data = gameData11;
            this.myButton30.FlatAppearance.BorderSize = 0;
            this.myButton30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton30.isSet = false;
            this.myButton30.Location = new System.Drawing.Point(97, 653);
            this.myButton30.Name = "myButton30";
            this.myButton30.Size = new System.Drawing.Size(80, 80);
            this.myButton30.TabIndex = 38;
            this.myButton30.UseVisualStyleBackColor = false;
            // 
            // myButton31
            // 
            this.myButton31.BackColor = System.Drawing.Color.Transparent;
            this.myButton31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton31.BackgroundImage")));
            this.myButton31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton31.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData12.angle = 0;
            gameData12.c = System.Drawing.Color.DarkGray;
            gameData12.dynamic = false;
            gameData12.h = 40;
            gameData12.hp = 100;
            gameData12.image = "level2.png";
            gameData12.nDamage = 0;
            gameData12.nDestroy = 0;
            gameData12.nImpact = 0;
            gameData12.numberOftextures = 0;
            gameData12.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData12.onDamage = null;
            gameData12.onDestroy = null;
            gameData12.onImpact = null;
            gameData12.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData12.w = 40;
            gameData12.x = 0;
            gameData12.y = 0;
            gameData12.z = 0;
            this.myButton31.data = gameData12;
            this.myButton31.FlatAppearance.BorderSize = 0;
            this.myButton31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton31.isSet = false;
            this.myButton31.Location = new System.Drawing.Point(97, 995);
            this.myButton31.Name = "myButton31";
            this.myButton31.Size = new System.Drawing.Size(80, 80);
            this.myButton31.TabIndex = 37;
            this.myButton31.UseVisualStyleBackColor = false;
            // 
            // myButton32
            // 
            this.myButton32.BackColor = System.Drawing.Color.Transparent;
            this.myButton32.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton32.BackgroundImage")));
            this.myButton32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton32.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData13.angle = 0;
            gameData13.c = System.Drawing.Color.DarkGray;
            gameData13.dynamic = false;
            gameData13.h = 40;
            gameData13.hp = 100;
            gameData13.image = "level1.png";
            gameData13.nDamage = 0;
            gameData13.nDestroy = 0;
            gameData13.nImpact = 0;
            gameData13.numberOftextures = 0;
            gameData13.objectType = LevelDesigner.E_BODY_TYPE.EBT_button;
            gameData13.onDamage = null;
            gameData13.onDestroy = null;
            gameData13.onImpact = null;
            gameData13.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData13.w = 40;
            gameData13.x = 0;
            gameData13.y = 0;
            gameData13.z = 0;
            this.myButton32.data = gameData13;
            this.myButton32.FlatAppearance.BorderSize = 0;
            this.myButton32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton32.isSet = false;
            this.myButton32.Location = new System.Drawing.Point(11, 995);
            this.myButton32.Name = "myButton32";
            this.myButton32.Size = new System.Drawing.Size(80, 80);
            this.myButton32.TabIndex = 36;
            this.myButton32.UseVisualStyleBackColor = false;
            // 
            // myButton14
            // 
            this.myButton14.BackColor = System.Drawing.Color.Transparent;
            this.myButton14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton14.BackgroundImage")));
            this.myButton14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton14.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData14.angle = 0;
            gameData14.c = System.Drawing.Color.DarkGray;
            gameData14.dynamic = true;
            gameData14.h = 40;
            gameData14.hp = 70;
            gameData14.image = "steklo_palica.png";
            gameData14.nDamage = 3;
            gameData14.nDestroy = 3;
            gameData14.nImpact = 8;
            gameData14.numberOftextures = 4;
            gameData14.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData14.onDamage = "glass damage a";
            gameData14.onDestroy = "glass destroy a";
            gameData14.onImpact = "glass collision a";
            gameData14.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData14.w = 40;
            gameData14.x = 0;
            gameData14.y = 0;
            gameData14.z = 0;
            this.myButton14.data = gameData14;
            this.myButton14.FlatAppearance.BorderSize = 0;
            this.myButton14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton14.isSet = false;
            this.myButton14.Location = new System.Drawing.Point(9, 440);
            this.myButton14.Name = "myButton14";
            this.myButton14.Size = new System.Drawing.Size(200, 16);
            this.myButton14.TabIndex = 34;
            this.myButton14.UseVisualStyleBackColor = false;
            // 
            // myButton15
            // 
            this.myButton15.BackColor = System.Drawing.Color.Transparent;
            this.myButton15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton15.BackgroundImage")));
            this.myButton15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton15.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData15.angle = 0;
            gameData15.c = System.Drawing.Color.DarkGray;
            gameData15.dynamic = true;
            gameData15.h = 40;
            gameData15.hp = 70;
            gameData15.image = "steklo_kvader.png";
            gameData15.nDamage = 3;
            gameData15.nDestroy = 3;
            gameData15.nImpact = 8;
            gameData15.numberOftextures = 4;
            gameData15.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData15.onDamage = "glass damage a";
            gameData15.onDestroy = "glass destroy a";
            gameData15.onImpact = "glass collision a";
            gameData15.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData15.w = 40;
            gameData15.x = 0;
            gameData15.y = 0;
            gameData15.z = 0;
            this.myButton15.data = gameData15;
            this.myButton15.FlatAppearance.BorderSize = 0;
            this.myButton15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton15.isSet = false;
            this.myButton15.Location = new System.Drawing.Point(113, 382);
            this.myButton15.Name = "myButton15";
            this.myButton15.Size = new System.Drawing.Size(83, 42);
            this.myButton15.TabIndex = 33;
            this.myButton15.UseVisualStyleBackColor = false;
            // 
            // myButton16
            // 
            this.myButton16.BackColor = System.Drawing.Color.Transparent;
            this.myButton16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton16.BackgroundImage")));
            this.myButton16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton16.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData16.angle = 0;
            gameData16.c = System.Drawing.Color.DarkGray;
            gameData16.dynamic = true;
            gameData16.h = 40;
            gameData16.hp = 70;
            gameData16.image = "steklo_kocka_praz.png";
            gameData16.nDamage = 3;
            gameData16.nDestroy = 3;
            gameData16.nImpact = 8;
            gameData16.numberOftextures = 4;
            gameData16.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData16.onDamage = "glass damage a";
            gameData16.onDestroy = "glass destroy a";
            gameData16.onImpact = "glass collision a";
            gameData16.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData16.w = 40;
            gameData16.x = 0;
            gameData16.y = 0;
            gameData16.z = 0;
            this.myButton16.data = gameData16;
            this.myButton16.FlatAppearance.BorderSize = 0;
            this.myButton16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton16.isSet = false;
            this.myButton16.Location = new System.Drawing.Point(9, 227);
            this.myButton16.Name = "myButton16";
            this.myButton16.Size = new System.Drawing.Size(40, 40);
            this.myButton16.TabIndex = 32;
            this.myButton16.UseVisualStyleBackColor = false;
            // 
            // myButton10
            // 
            this.myButton10.BackColor = System.Drawing.Color.Transparent;
            this.myButton10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton10.BackgroundImage")));
            this.myButton10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton10.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData17.angle = 0;
            gameData17.c = System.Drawing.Color.DarkGray;
            gameData17.dynamic = true;
            gameData17.h = 40;
            gameData17.hp = 150;
            gameData17.image = "les_palica.png";
            gameData17.nDamage = 3;
            gameData17.nDestroy = 3;
            gameData17.nImpact = 6;
            gameData17.numberOftextures = 4;
            gameData17.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData17.onDamage = "wood damage a";
            gameData17.onDestroy = "wood destroy a";
            gameData17.onImpact = "wood collision a";
            gameData17.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData17.w = 40;
            gameData17.x = 0;
            gameData17.y = 0;
            gameData17.z = 0;
            this.myButton10.data = gameData17;
            this.myButton10.FlatAppearance.BorderSize = 0;
            this.myButton10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton10.isSet = false;
            this.myButton10.Location = new System.Drawing.Point(9, 462);
            this.myButton10.Name = "myButton10";
            this.myButton10.Size = new System.Drawing.Size(200, 16);
            this.myButton10.TabIndex = 30;
            this.myButton10.UseVisualStyleBackColor = false;
            // 
            // myButton11
            // 
            this.myButton11.BackColor = System.Drawing.Color.Transparent;
            this.myButton11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton11.BackgroundImage")));
            this.myButton11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton11.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData18.angle = 0;
            gameData18.c = System.Drawing.Color.DarkGray;
            gameData18.dynamic = true;
            gameData18.h = 40;
            gameData18.hp = 150;
            gameData18.image = "les_krog.png";
            gameData18.nDamage = 3;
            gameData18.nDestroy = 3;
            gameData18.nImpact = 6;
            gameData18.numberOftextures = 4;
            gameData18.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData18.onDamage = "wood damage a";
            gameData18.onDestroy = "wood destroy a";
            gameData18.onImpact = "wood impact a";
            gameData18.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_circle;
            gameData18.w = 40;
            gameData18.x = 0;
            gameData18.y = 0;
            gameData18.z = 0;
            this.myButton11.data = gameData18;
            this.myButton11.FlatAppearance.BorderSize = 0;
            this.myButton11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton11.isSet = false;
            this.myButton11.Location = new System.Drawing.Point(55, 227);
            this.myButton11.Name = "myButton11";
            this.myButton11.Size = new System.Drawing.Size(40, 40);
            this.myButton11.TabIndex = 29;
            this.myButton11.UseVisualStyleBackColor = false;
            // 
            // myButton12
            // 
            this.myButton12.BackColor = System.Drawing.Color.Transparent;
            this.myButton12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton12.BackgroundImage")));
            this.myButton12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton12.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData19.angle = 0;
            gameData19.c = System.Drawing.Color.DarkGray;
            gameData19.dynamic = true;
            gameData19.h = 40;
            gameData19.hp = 150;
            gameData19.image = "les_kocka_praz.png";
            gameData19.nDamage = 3;
            gameData19.nDestroy = 3;
            gameData19.nImpact = 6;
            gameData19.numberOftextures = 4;
            gameData19.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData19.onDamage = "wood damage a";
            gameData19.onDestroy = "wood destroy a";
            gameData19.onImpact = "wood collision a";
            gameData19.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData19.w = 40;
            gameData19.x = 0;
            gameData19.y = 0;
            gameData19.z = 0;
            this.myButton12.data = gameData19;
            this.myButton12.FlatAppearance.BorderSize = 0;
            this.myButton12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton12.isSet = false;
            this.myButton12.Location = new System.Drawing.Point(104, 168);
            this.myButton12.Name = "myButton12";
            this.myButton12.Size = new System.Drawing.Size(40, 40);
            this.myButton12.TabIndex = 28;
            this.myButton12.UseVisualStyleBackColor = false;
            // 
            // myButton7
            // 
            this.myButton7.BackColor = System.Drawing.Color.Transparent;
            this.myButton7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton7.BackgroundImage")));
            this.myButton7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton7.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData20.angle = 0;
            gameData20.c = System.Drawing.Color.DarkGray;
            gameData20.dynamic = true;
            gameData20.h = 40;
            gameData20.hp = 300;
            gameData20.image = "kamen_palica.png";
            gameData20.nDamage = 3;
            gameData20.nDestroy = 3;
            gameData20.nImpact = 5;
            gameData20.numberOftextures = 4;
            gameData20.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData20.onDamage = "rock damage a";
            gameData20.onDestroy = "rock destroy a";
            gameData20.onImpact = "rock collision a";
            gameData20.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData20.w = 40;
            gameData20.x = 0;
            gameData20.y = 0;
            gameData20.z = 0;
            this.myButton7.data = gameData20;
            this.myButton7.FlatAppearance.BorderSize = 0;
            this.myButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton7.isSet = false;
            this.myButton7.Location = new System.Drawing.Point(9, 484);
            this.myButton7.Name = "myButton7";
            this.myButton7.Size = new System.Drawing.Size(200, 16);
            this.myButton7.TabIndex = 26;
            this.myButton7.UseVisualStyleBackColor = false;
            // 
            // myButton6
            // 
            this.myButton6.BackColor = System.Drawing.Color.Transparent;
            this.myButton6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton6.BackgroundImage")));
            this.myButton6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton6.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData21.angle = 0;
            gameData21.c = System.Drawing.Color.DarkGray;
            gameData21.dynamic = true;
            gameData21.h = 40;
            gameData21.hp = 300;
            gameData21.image = "kamen_kvader.png";
            gameData21.nDamage = 3;
            gameData21.nDestroy = 3;
            gameData21.nImpact = 5;
            gameData21.numberOftextures = 4;
            gameData21.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData21.onDamage = "rock damage a";
            gameData21.onDestroy = "rock destroy a";
            gameData21.onImpact = "rock collision a";
            gameData21.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData21.w = 40;
            gameData21.x = 0;
            gameData21.y = 0;
            gameData21.z = 0;
            this.myButton6.data = gameData21;
            this.myButton6.FlatAppearance.BorderSize = 0;
            this.myButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton6.isSet = false;
            this.myButton6.Location = new System.Drawing.Point(113, 334);
            this.myButton6.Name = "myButton6";
            this.myButton6.Size = new System.Drawing.Size(83, 42);
            this.myButton6.TabIndex = 25;
            this.myButton6.UseVisualStyleBackColor = false;
            // 
            // myButton5
            // 
            this.myButton5.BackColor = System.Drawing.Color.Transparent;
            this.myButton5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton5.BackgroundImage")));
            this.myButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData22.angle = 0;
            gameData22.c = System.Drawing.Color.DarkGray;
            gameData22.dynamic = true;
            gameData22.h = 40;
            gameData22.hp = 300;
            gameData22.image = "kamen_krog.png";
            gameData22.nDamage = 3;
            gameData22.nDestroy = 3;
            gameData22.nImpact = 5;
            gameData22.numberOftextures = 4;
            gameData22.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData22.onDamage = "rock damage a";
            gameData22.onDestroy = "rock destroy a";
            gameData22.onImpact = "rock collision a";
            gameData22.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_circle;
            gameData22.w = 40;
            gameData22.x = 0;
            gameData22.y = 0;
            gameData22.z = 0;
            this.myButton5.data = gameData22;
            this.myButton5.FlatAppearance.BorderSize = 0;
            this.myButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton5.isSet = false;
            this.myButton5.Location = new System.Drawing.Point(104, 227);
            this.myButton5.Name = "myButton5";
            this.myButton5.Size = new System.Drawing.Size(40, 40);
            this.myButton5.TabIndex = 24;
            this.myButton5.UseVisualStyleBackColor = false;
            // 
            // myButton3
            // 
            this.myButton3.BackColor = System.Drawing.Color.Transparent;
            this.myButton3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton3.BackgroundImage")));
            this.myButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData23.angle = 0;
            gameData23.c = System.Drawing.Color.DarkGray;
            gameData23.dynamic = true;
            gameData23.h = 40;
            gameData23.hp = 300;
            gameData23.image = "kamen_kocka_praz.png";
            gameData23.nDamage = 3;
            gameData23.nDestroy = 3;
            gameData23.nImpact = 3;
            gameData23.numberOftextures = 4;
            gameData23.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData23.onDamage = "rock damage a";
            gameData23.onDestroy = "rock destroy a";
            gameData23.onImpact = "rock collision a";
            gameData23.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData23.w = 40;
            gameData23.x = 0;
            gameData23.y = 0;
            gameData23.z = 0;
            this.myButton3.data = gameData23;
            this.myButton3.FlatAppearance.BorderSize = 0;
            this.myButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton3.isSet = false;
            this.myButton3.Location = new System.Drawing.Point(158, 169);
            this.myButton3.Name = "myButton3";
            this.myButton3.Size = new System.Drawing.Size(40, 40);
            this.myButton3.TabIndex = 23;
            this.myButton3.UseVisualStyleBackColor = false;
            // 
            // myButton4
            // 
            this.myButton4.BackColor = System.Drawing.Color.Transparent;
            this.myButton4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton4.BackgroundImage")));
            this.myButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData24.angle = 0;
            gameData24.c = System.Drawing.Color.DarkGray;
            gameData24.dynamic = true;
            gameData24.h = 40;
            gameData24.hp = 100;
            gameData24.image = "zoga1.png";
            gameData24.nDamage = 0;
            gameData24.nDestroy = 0;
            gameData24.nImpact = 0;
            gameData24.numberOftextures = 0;
            gameData24.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData24.onDamage = null;
            gameData24.onDestroy = null;
            gameData24.onImpact = null;
            gameData24.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_circle;
            gameData24.w = 40;
            gameData24.x = 0;
            gameData24.y = 0;
            gameData24.z = 0;
            this.myButton4.data = gameData24;
            this.myButton4.FlatAppearance.BorderSize = 0;
            this.myButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton4.isSet = false;
            this.myButton4.Location = new System.Drawing.Point(9, 286);
            this.myButton4.Name = "myButton4";
            this.myButton4.Size = new System.Drawing.Size(40, 40);
            this.myButton4.TabIndex = 22;
            this.myButton4.UseVisualStyleBackColor = false;
            // 
            // myButton2
            // 
            this.myButton2.BackColor = System.Drawing.Color.Transparent;
            this.myButton2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton2.BackgroundImage")));
            this.myButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData25.angle = 0;
            gameData25.c = System.Drawing.Color.DarkGray;
            gameData25.dynamic = true;
            gameData25.h = 40;
            gameData25.hp = 100;
            gameData25.image = "les_kocka2.png";
            gameData25.nDamage = 0;
            gameData25.nDestroy = 0;
            gameData25.nImpact = 3;
            gameData25.numberOftextures = 1;
            gameData25.objectType = LevelDesigner.E_BODY_TYPE.EBT_groundObject;
            gameData25.onDamage = null;
            gameData25.onDestroy = null;
            gameData25.onImpact = "wood damage a";
            gameData25.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData25.w = 40;
            gameData25.x = 0;
            gameData25.y = 0;
            gameData25.z = 0;
            this.myButton2.data = gameData25;
            this.myButton2.FlatAppearance.BorderSize = 0;
            this.myButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton2.isSet = false;
            this.myButton2.Location = new System.Drawing.Point(55, 168);
            this.myButton2.Name = "myButton2";
            this.myButton2.Size = new System.Drawing.Size(40, 40);
            this.myButton2.TabIndex = 21;
            this.myButton2.UseVisualStyleBackColor = false;
            // 
            // myButton1
            // 
            this.myButton1.BackColor = System.Drawing.Color.Transparent;
            this.myButton1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("myButton1.BackgroundImage")));
            this.myButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            gameData26.angle = 0;
            gameData26.c = System.Drawing.Color.DarkGray;
            gameData26.dynamic = true;
            gameData26.h = 40;
            gameData26.hp = 150;
            gameData26.image = "les_kocka.png";
            gameData26.nDamage = 3;
            gameData26.nDestroy = 3;
            gameData26.nImpact = 6;
            gameData26.numberOftextures = 2;
            gameData26.objectType = LevelDesigner.E_BODY_TYPE.EBT_obstacle;
            gameData26.onDamage = "wood damage a";
            gameData26.onDestroy = "wood destroy a";
            gameData26.onImpact = "wood collision a";
            gameData26.shapeType = LevelDesigner.E_SHAPE_TYPE.EST_rect;
            gameData26.w = 40;
            gameData26.x = 0;
            gameData26.y = 0;
            gameData26.z = 0;
            this.myButton1.data = gameData26;
            this.myButton1.FlatAppearance.BorderSize = 0;
            this.myButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.myButton1.isSet = false;
            this.myButton1.Location = new System.Drawing.Point(9, 168);
            this.myButton1.Name = "myButton1";
            this.myButton1.Size = new System.Drawing.Size(40, 40);
            this.myButton1.TabIndex = 20;
            this.myButton1.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 670);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Level Designer";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem datotekaToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox t_height;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox t_width;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox t_posY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox t_posX;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private Button button1;
        private ToolStripMenuItem shraniToolStripMenuItem;
        private ToolStripMenuItem naložiToolStripMenuItem;
        private Button button2;
        private ColorDialog colorDialog1;
        private ToolStripMenuItem dodatnoToolStripMenuItem;
        private ToolStripMenuItem ozadjeToolStripMenuItem;
        private OpenFileDialog openFileDialog1;
        private Label label9;
        private Label label10;
        private ComboBox t_dynamic;
        private ComboBox t_gravity;
        private SaveFileDialog saveFileDialog1;
        private myButton myButton14;
        private myButton myButton15;
        private myButton myButton16;
        private myButton myButton10;
        private myButton myButton11;
        private myButton myButton12;
        private myButton myButton7;
        private myButton myButton6;
        private myButton myButton5;
        private myButton myButton3;
        private myButton myButton4;
        private myButton myButton2;
        private myButton myButton1;
        private Label label7;
        private myButton myButton19;
        private myButton myButton18;
        private myButton myButton17;
        private myButton myButton20;
        private myButton myButton23;
        private myButton myButton24;
        private myButton myButton28;
        private myButton myButton29;
        private myButton myButton30;
        private myButton myButton31;
        private myButton myButton32;
        private ComboBox t_type;
        private Label label8;
        private ComboBox t_shape;
        private Label label11;
        private ToolStripMenuItem novoToolStripMenuItem;
        private CheckBox checkBox1;
        private ToolStripMenuItem shraniKotToolStripMenuItem;
        private ToolStripMenuItem dodajObjektToolStripMenuItem;
        private myButton myButton22;
        private myButton myButton8;
    }
}

