﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace LevelDesigner
{
    class myButton : Button, ICloneable
    {
        [Category("A_myStuff")]
        public GameData data { get; set; }
        [Category("A_myStuff")]
        public bool isSet { get; set; }

        public myButton()
        {
            BackgroundImageLayout = ImageLayout.Stretch;
            FlatAppearance.BorderSize = 0;
            FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            isSet = false;
            data = new GameData();
            Cursor = Cursors.Hand;
            data.image = "";
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //Graphics g = e.Graphics;
            //Brush _brush = new SolidBrush(data.c);
            //g.FillRectangle(_brush, 0, 0, ClientSize.Width, ClientSize.Height);
            base.OnPaint(e);
        }

        protected override bool ShowFocusCues
        {
            get
            {
                return false;
            }
        }


        public object Clone()
        {
            myButton x = new myButton();

            x.Size = Size;
            x.BackColor = BackColor;
            x.isSet = true;
            x.data = (GameData)data.Clone();
            x.BackgroundImage = BackgroundImage;

            return x;
        }
    }

    public enum E_BODY_TYPE
    {
        EBT_undefined,
        EBT_tossingBall,
        EBT_enemy,
        EBT_sceneObject,
        EBT_groundObject,
        EBT_obstacle,
        EBT_button
    };

    public enum E_SHAPE_TYPE
    {
        EST_rect,
        EST_circle,
        EST_triangle
    }

    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class GameData : ICloneable
    {
        // basic data
        public int x { get; set; } // x position
        public int y { get; set; } // y position
        public int z { get; set; } // z position

        public int w { get; set; } // width
        public int h { get; set; } // height

        public int hp { get; set; } // hit points

        public int angle { get; set; } // angle in degrees

        // color
        public Color c { get; set; }

        // entity type
        public E_BODY_TYPE objectType { get; set; }
        // shape type
        public E_SHAPE_TYPE shapeType { get; set; }
        
        // texutres
        public string   image { get; set; } // default texture
        public int      numberOftextures { get; set; }

        // physics data
        public bool dynamic { get; set; }
        public bool gravity;

        // sounds
        public string   onImpact  { get; set; }
        public string   onDamage  { get; set; }
        public string   onDestroy { get; set; }
        public int      nImpact  { get; set; }
        public int      nDamage  { get; set; }
        public int      nDestroy { get; set; }

        public GameData()
        {
            image = "";
            numberOftextures = 0;
            hp = 100;
        }

        public override String ToString()
        {
            return "" + x + "|" + y + "|" + w + "|" + h + "|" + c.ToArgb() + "|" + (int)objectType + "|" + image + "|" + dynamic.ToString() + "|" + (int)shapeType;
        }


        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    class Path
    {
        public Point a { get; set; }
        public Point b { get; set; }
        public myButton buttonRef { get; set; }
        public int velocity { get; set; }

        public Path()
        {
            a = new Point(-1, -1);
            b = new Point(-1, -1);
            buttonRef = null;
            velocity = 0;
        }

        public void Render(Graphics g)
        {
            if (a.X != -1 && b.X != -1)
            {
                {
                    g.DrawLine(new Pen(Color.Black), a, b);
                }
            }
        }
    }
}
