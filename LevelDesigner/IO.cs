﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Xml;

namespace LevelDesigner
{
    class IO
    {
        public static bool SaveToXML(List<GameData> data, String path, bool gui)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.NewLineOnAttributes = true;
            using (XmlWriter writer = XmlWriter.Create(path, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Level");
                
                foreach (GameData gd in data)
                {
                    writer.WriteStartElement("Object");

                    writer.WriteAttributeString("Position-x", "" + gd.x);
                    writer.WriteAttributeString("Position-y", "" + gd.y);
                    writer.WriteAttributeString("Width", "" + gd.w);
                    writer.WriteAttributeString("Height", "" + gd.h);
                    writer.WriteAttributeString("Image", "" + gd.image);
                    writer.WriteAttributeString("ObjectType", "" + (int)gd.objectType);

                    if (!gui)
                    {
                        writer.WriteAttributeString("HitPoints", "" + gd.hp);

                        writer.WriteAttributeString("NumberOfTextures", "" + gd.numberOftextures);

                        writer.WriteAttributeString("OnImpactSound", gd.onImpact);
                        writer.WriteAttributeString("OnDamageSound", gd.onDamage);
                        writer.WriteAttributeString("OnDestroySound", gd.onDestroy);

                        writer.WriteAttributeString("nImpact", "" + (int)gd.nImpact);
                        writer.WriteAttributeString("nDamage", "" + (int)gd.nDamage);
                        writer.WriteAttributeString("nDestroy", "" + (int)gd.nDestroy);

                        writer.WriteAttributeString("ShapeType", "" + (int)gd.shapeType);

                        writer.WriteAttributeString("Dynamic", gd.dynamic.ToString());
                    }

                        
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
            return true;
        }

        public static List<GameData> LoadFromXML(String path, bool gui)
        {
            List<GameData> gdata = new List<GameData>();

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ConformanceLevel = ConformanceLevel.Document;

            using (XmlReader reader = XmlReader.Create(path, settings))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Level":
                                break;

                            case "Object":

                                GameData g = new GameData();

                                g.x = Int32.Parse(reader["Position-x"]);
                                g.y = Int32.Parse(reader["Position-y"]);
                                //g.z = Int32.Parse(reader["Position-z"]);

                                g.w = Int32.Parse(reader["Width"]);
                                g.h = Int32.Parse(reader["Height"]);

                                g.image = reader["Image"];

                                if (!gui)
                                {
                                    g.hp = Int32.Parse(reader["HitPoints"]);

                                    g.numberOftextures = Int32.Parse(reader["NumberOfTextures"]);

                                    g.onImpact  = reader["OnImpactSound"];
                                    g.onDamage  = reader["OnDamageSound"];
                                    g.onDestroy = reader["OnDestroySound"];
                                    g.nImpact = Int32.Parse(reader["nImpact"]);
                                    g.nDamage = Int32.Parse(reader["nDamage"]);
                                    g.nDestroy = Int32.Parse(reader["nDestroy"]);

                                    g.objectType = (E_BODY_TYPE)Int32.Parse(reader["ObjectType"]);
                                    g.shapeType = (E_SHAPE_TYPE)Int32.Parse(reader["ShapeType"]);

                                    g.dynamic = (reader["Dynamic"].Equals("True"));
                                }

                                gdata.Add(g);

                                break;
                        }
                    }
                }
            }

            return gdata;
        }
    }
}
